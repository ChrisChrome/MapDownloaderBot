const fs = require('fs');
const unzipper = require('unzipper');
const download = require('download');
const config = require('./config.js');
const request = require('request');
const https = require('https');
const Discord = require('discord.js');
const bot = new Discord.Client();

//Snippets
function secondsToTime(secs) {
    var hours = Math.floor(secs / (60 * 60));

    var divisor_for_minutes = secs % (60 * 60);
    var minutes = Math.floor(divisor_for_minutes / 60);

    var divisor_for_seconds = divisor_for_minutes % 60;
    var seconds = Math.ceil(divisor_for_seconds);

    var obj = {
        "h": hours,
        "m": minutes,
        "s": seconds
    };
    return obj;
}

bot.on('ready', () => {
    console.log(`Logged in as ${bot.user.tag}`);
});

bot.on('message', (msg) => {
    const prefix = config.discord.prefix;
    const args = msg.content.slice(prefix.length).trim().split(/ +/g);
    const cmd = args.shift().toLowerCase();
    if (msg.content.toLowerCase().startsWith(prefix)) {
        switch (cmd) {
            case "dl":
                if (!config.discord.downloadenabled) return;
                var songData = null;
                if (!args[0]) return msg.channel.send(`You need to provide a valid BeatSaver song key!\n${config.discord.prefix}dl <key>`);
                if (!/^[a-zA-Z0-9]{2,7}$/.test(args[0])) return msg.channel.send(`Song key invalid!`);
                request({
                    url: `https://beatsaver.com/api/maps/detail/${args[0]}`,
                    headers: {
                        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0',
                        'Accept': 'application/json',
                        'Accept-Language': 'en-US,en;q=0.5',
                        'Alt-Used': 'beatsaver.com',
                        'Upgrade-Insecure-Requests': '1',
                        'Sec-GPC': '1',
                        'Cache-Control': 'max-age=0'
                    }
                    //json: true
                }, (err, res, body) => {
                    if (res.statusCode === 404) return msg.channel.send(`Song not found!`);
                    if (!err && res.statusCode === 200) {
                        songData = JSON.parse(body);
                        duration = secondsToTime(songData.metadata.duration);
                        msg.channel.send({
                            embed: {
                                title: `Downloading...`,
                                description: `[${songData.name}](https://beatsaver.com/beatmap/${songData.key})\n${songData.description}`,
                                timestamp: songData.uploaded,
                                thumbnail: {
                                    url: `https://beatsaver.com${songData.coverURL}`
                                },
                                footer: {
                                    text: `Uploaded by ${songData.uploader.username} at`
                                },
                                fields: [{
                                        name: "Details",
                                        value: `${duration.m}:${duration.s} :clock5:\n${songData.key} :key:\n${songData.stats.downloads} :floppy_disk:\n${songData.stats.upVotes} :thumbsup:\n${songData.stats.downVotes} :thumbsdown:\n%${(songData.stats.rating*100).toString().substr(0,4)} :100:`
                                    },
                                    {
                                        name: "Difficulties",
                                        value: `${songData.metadata.difficulties.easy ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Easy\n${songData.metadata.difficulties.normal ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Normal\n${songData.metadata.difficulties.hard ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Hard\n${songData.metadata.difficulties.expert ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Expert\n${songData.metadata.difficulties.expertPlus ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Expert+`
                                    }
                                ]
                            }
                        }).then((msg1) => {
                            if (fs.existsSync(`${config.beatsaver.downloadpath}/${songData.key} (${songData.metadata.songName} - ${songData.metadata.levelAuthorName})`)) {
                                tmpEmbed = msg1.embeds[0];
                                tmpEmbed.title = "Already Downloaded!"
                                return msg1.edit({
                                    embed: tmpEmbed
                                });
                            }
                            download(`https://beatsaver.com/api/download/key/${songData.key}`, `${config.beatsaver.downloadpath}`).then(() => {
                                fs.createReadStream(`${config.beatsaver.downloadpath}/${songData.key}.zip`).pipe(unzipper.Extract({
                                    path: `${config.beatsaver.downloadpath}/${songData.key} (${songData.metadata.songName} - ${songData.metadata.levelAuthorName})`
                                }))
                                fs.unlink(`${config.beatsaver.downloadpath}/${songData.key}.zip`, () => {
                                    tmpEmbed = msg1.embeds[0];
                                    tmpEmbed.title = "Download Complete!";
                                    return msg1.edit({
                                        embed: tmpEmbed
                                    });
                                });
                            })
                        })
                    }
                })
                break;
            case "info":
                var songData = null;
                if (!args[0]) return msg.channel.send(`You need to provide a valid BeatSaver song key!\n${config.discord.prefix}info <key>`);
                if (!/^[a-zA-Z0-9]{2,7}$/.test(args[0])) return msg.channel.send(`Song key invalid!`);
                request({
                    url: `https://beatsaver.com/api/maps/detail/${args[0]}`,
                    headers: {
                        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0',
                        'Accept': 'application/json',
                        'Accept-Language': 'en-US,en;q=0.5',
                        'Alt-Used': 'beatsaver.com',
                        'Upgrade-Insecure-Requests': '1',
                        'Sec-GPC': '1',
                        'Cache-Control': 'max-age=0'
                    }
                    //json: true
                }, (err, res, body) => {
                    if (res.statusCode === 404) return msg.channel.send(`Song not found!`);
                    if (!err && res.statusCode === 200) {
                        songData = JSON.parse(body);
                        duration = secondsToTime(songData.metadata.duration);
                        msg.channel.send({
                            embed: {
                                title: `${songData.name}`,
                                description: `[${songData.name}](https://beatsaver.com/beatmap/${songData.key})\n${songData.description}`,
                                timestamp: songData.uploaded,
                                thumbnail: {
                                    url: `https://beatsaver.com${songData.coverURL}`
                                },
                                footer: {
                                    text: `Uploaded by ${songData.uploader.username} at`
                                },
                                fields: [{
                                        name: "Details",
                                        value: `${duration.m}:${duration.s} :clock5:\n${songData.key} :key:\n${songData.stats.downloads} :floppy_disk:\n${songData.stats.upVotes} :thumbsup:\n${songData.stats.downVotes} :thumbsdown:\n%${(songData.stats.rating*100).toString().substr(0,4)} :100:`
                                    },
                                    {
                                        name: "Difficulties",
                                        value: `${songData.metadata.difficulties.easy ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Easy\n${songData.metadata.difficulties.normal ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Normal\n${songData.metadata.difficulties.hard ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Hard\n${songData.metadata.difficulties.expert ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Expert\n${songData.metadata.difficulties.expertPlus ? ":white_check_mark:" : ":negative_squared_cross_mark:"} Expert+`
                                    }
                                ]
                            }
                        })
                    }
                })
                break;
        }
    }
});

bot.login(config.discord.token);