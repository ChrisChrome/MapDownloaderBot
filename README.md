MapDownloaderBot

> Note to self: write a proper README file

Default config.js file
```JavaScript
module.exports = {
    discord: {
        token: "", //Put your Discord bot token here, you can get one at https://discord.com/developers/applications
        prefix: "!", //The prefix for commands, default is !
        downloadenabled: true //Whether the dl command is enabled or not.
    },
    beatsaver: {
        downloadpath: "./Beat Saber_Data/CustomLevels" //This assumes you put the files in, and are launching from the Beat Saber game directory
    }
};
```